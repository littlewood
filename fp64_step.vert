/*
    littlewood -- GPU accelerated Littlewood fractal renderer
    Copyright (C) 2012  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#version 400 core
#extension GL_ARB_vertex_attrib_64bit : enable
#extension GL_ARB_separate_shader_objects : enable
precision highp float;

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

layout(location = POSITION) in dvec4 Position;
layout(location = COLOR) in dvec4 Color;

out block {
layout(location = POSITION)   dvec4 Position;
layout(location = COLOR)   dvec4 Color;
} Out;

uniform double plusMinus;
uniform double hueShift;

void main() {
  dvec2 z = Position.xy;
  dvec2 zn = Position.zw;
  dvec2 p = Color.xy;
  double h = Color.z;
  p += plusMinus * zn;
  h += hueShift;
  zn = dvec2(z.x * zn.x - z.y * zn.y, z.x * zn.y + z.y * zn.x);
  Out.Position = dvec4(z, zn);
  Out.Color = dvec4(p, h, 0.0);
}
