/*
    littlewood -- GPU accelerated Littlewood fractal renderer
    Copyright (C) 2012  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#version 400 core
#extension GL_ARB_vertex_attrib_64bit : enable
#extension GL_ARB_separate_shader_objects : enable
precision highp float;

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

in block {
  layout(location = COLOR) smooth vec4 TexCoord;
} In;

layout(location = FRAG_COLOR, index = 0) out vec4 Color;

uniform sampler2D tex;

void main() {
  vec4 c = texture2D(tex, In.TexCoord.xy);
  float y = c.r;
  float u = c.g;
  float v = c.b;
  if (y > 0.0) {
    float k = log2(y + 1.0) / y;
    k /= 16.0;
    y *= k;
    u *= k;
    v *= k;
  }
  vec3 rgb = vec3(y + 1.28033 * v, y - 0.21482 * u - 0.38059 * v, y + 2.12798 * u);
  Color = vec4(clamp(rgb, 0.0, 1.0), 1.0);
}
