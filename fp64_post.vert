/*
    littlewood -- GPU accelerated Littlewood fractal renderer
    Copyright (C) 2012  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#version 400 core
#extension GL_ARB_vertex_attrib_64bit : enable
#extension GL_ARB_separate_shader_objects : enable
precision highp float;

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

uniform dmat4 MVP;

layout(location = POSITION) in vec4 Position;

out block {
  layout(location = COLOR) vec4 TexCoord;
} Out;

void main() {
  gl_Position = vec4(mat4(MVP) * vec4(Position.xy, 0.0, 1.0));
  Out.TexCoord = vec4(Position.xy, 0.0, 1.0);
}
