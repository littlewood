/*
    littlewood -- GPU accelerated Littlewood fractal renderer
    Copyright (C) 2012,2015,2017  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#version 330 core
#extension GL_ARB_separate_shader_objects : enable
precision highp float;

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

layout(points) in;
layout(points, max_vertices = 1) out;

layout(location = POSITION) in vec4 OutPosition[1];
layout(location = COLOR) in vec4 OutColor[1];

layout(location = POSITION) out vec4 Position;
layout(location = COLOR) out vec4 Color;

void main() {
  Position = OutPosition[0];
  Color = OutColor[0];
  EmitVertex();
  EndPrimitive();
}
