/*
    littlewood -- GPU accelerated Littlewood fractal renderer
    Copyright (C) 2012  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#version 330 core
precision highp float;

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

uniform mat4 MVP;

layout(location = POSITION) in vec4 Position;
layout(location = COLOR) in vec4 Color;

out block {
  vec4 Color;
} Out;

void main() {
  gl_Position = MVP * vec4(Position.xy, 0.0, 1.0);
  float h = Color.z;
  float y = 0.5;
  float u = 0.25 * cos(h);
  float v = 0.25 * sin(h);
  Out.Color = vec4(y, u, v, 1.0);
}
