/*
    littlewood -- GPU accelerated Littlewood fractal renderer
    Copyright (C) 2012,2015,2017  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#version 330 core
#extension GL_ARB_separate_shader_objects : enable
precision highp float;

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

layout(location = POSITION) in vec4 Position;
layout(location = COLOR) in vec4 Color;

layout(location = POSITION) out vec4 OutPosition;
layout(location = COLOR) out vec4 OutColor;

uniform float plusMinus;
uniform float hueShift;

void main() {
  vec2 z = Position.xy;
  vec2 zn = Position.zw;
  vec2 p = Color.xy;
  float h = Color.z;
  p += plusMinus * zn;
  h += hueShift;
  zn = vec2(z.x * zn.x - z.y * zn.y, z.x * zn.y + z.y * zn.x);
  OutPosition = vec4(z, zn);
  OutColor = vec4(p, h, 0.0);
}
