#
#   littlewood -- GPU accelerated Littlewood fractal renderer
#   Copyright (C) 2012,2015,2017  Claude Heiland-Allen <claude@mathr.co.uk>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

LIBS = -lGLEW -lglut -lGL
CCFLAGS = -Wall -pedantic -Wextra -O3 -march=native -ggdb

all: littlewood-fp32 littlewood-fp64

clean:
	-rm littlewood-fp32 fp32_init0.frag.c fp32_init0.vert.c fp32_init1.geom.c fp32_init1.vert.c fp32_step.geom.c fp32_step.vert.c fp32_draw.frag.c fp32_draw.vert.c fp32_post.frag.c fp32_post.vert.c
	-rm littlewood-fp64 fp64_init0.frag.c fp64_init0.vert.c fp64_init1.geom.c fp64_init1.vert.c fp64_step.geom.c fp64_step.vert.c fp64_draw.frag.c fp64_draw.vert.c fp64_post.frag.c fp64_post.vert.c

.SUFFIXES:
.PHONY: all clean

littlewood-fp32: littlewood.cc fp32_init0.frag.c fp32_init0.vert.c fp32_init1.geom.c fp32_init1.vert.c fp32_step.geom.c fp32_step.vert.c fp32_draw.frag.c fp32_draw.vert.c fp32_post.frag.c fp32_post.vert.c
	g++ $(CCFLAGS) -DFP=32 -o littlewood-fp32 littlewood.cc $(LIBS)

littlewood-fp64: littlewood.cc fp64_init0.frag.c fp64_init0.vert.c fp64_init1.geom.c fp64_init1.vert.c fp64_step.geom.c fp64_step.vert.c fp64_draw.frag.c fp64_draw.vert.c fp64_post.frag.c fp64_post.vert.c
	g++ $(CCFLAGS) -DFP=64 -o littlewood-fp64 littlewood.cc $(LIBS)

# shader source to C source

%.frag.c: %.frag s2c.sh
	./s2c.sh $*_frag < $< > $@

%.geom.c: %.geom s2c.sh
	./s2c.sh $*_geom < $< > $@

%.vert.c: %.vert s2c.sh
	./s2c.sh $*_vert < $< > $@
