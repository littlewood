littlewood -- GPU accelerated Littlewood fractal renderer
Copyright (C) 2012,2015,2017  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


littlewood requires freeglut, glew, glm
littlewood-fp32 requires OpenGL 3.3
littlewood-fp64 requires OpenGL 4.0

left-click to zoom in
middle-click to center
right-click to zoom out
esc to quit

viewport parameters and statistics are printed on stderr:
centerx centery radiusy -log2(radiusy) roots order time


data flow:
    viewing parameters
->  init0 shader generates coordinates                    } float32
->  stored in framebuffer texture                         }
->  copy to vertex buffer                                 }
->  init1 shader augments coordinates with initial state  } floatNN
  loop until vertex buffer could overflow:                }
  ->  ping vertex buffer                                  }
  ->  step shader (twice, once with + and once with -)    }
  ->  pong vertex buffer                                  }
  ->  swap(ping, pong)                                    }
->  draw shader accumulates points in framebuffer texture } float32
->  post shader normalizes colour range                   }
->  output image                                          } uint8


could possibly be described as an approximantion to infinite-order
algorithm prunes polynomials that can't converge to 0 anywhere in the pixel
if a pixel is black, there is for sure no root nearby
pixel brightness is related to number of paths that haven't been pruned
pixel colour is related to the choices made at each step (+/-)


references:
http://www.gregegan.net/SCIENCE/Littlewood/Littlewood.html
http://johncarlosbaez.wordpress.com/2011/12/11/the-beauty-of-roots/
  (search comments for "prune")
