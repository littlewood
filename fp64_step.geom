/*
    littlewood -- GPU accelerated Littlewood fractal renderer
    Copyright (C) 2012,2017  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#version 400 core
#extension GL_ARB_vertex_attrib_64bit : enable
#extension GL_ARB_separate_shader_objects : enable
precision highp float;

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

layout(points) in;
layout(points, max_vertices = 1) out;

layout(location = POSITION) in dvec4 OutPosition[1];
layout(location = COLOR) in dvec4 OutColor[1];

uniform double pixelSpacing;

layout(location = POSITION) out dvec4 Position;
layout(location = COLOR) out dvec4 Color;

void main() {
  dvec2 z = OutPosition[0].xy;
  dvec2 zn = OutPosition[0].zw;
  dvec2 p = OutColor[0].xy;
  double r = length(z);
  double rn = length(zn);
  if (r <= 0.8 && length(p) <= rn / (1.0 - r) + pixelSpacing) {
    Position = OutPosition[0];
    Color = OutColor[0];
    EmitVertex();
    EndPrimitive();
  }
}
