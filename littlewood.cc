/*
    littlewood -- GPU accelerated Littlewood fractal renderer
    Copyright (C) 2012,2015,2017  Claude Heiland-Allen <claude@mathr.co.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FP
#define FP 32
#endif

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <stdio.h>
#include <time.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#if (FP == 32)

#include "fp32_init0.vert.c"
static const char *init0_vert = fp32_init0_vert;
#include "fp32_init0.frag.c"
static const char *init0_frag = fp32_init0_frag;
#include "fp32_init1.vert.c"
static const char *init1_vert = fp32_init1_vert;
#include "fp32_init1.geom.c"
static const char *init1_geom = fp32_init1_geom;
#include "fp32_step.vert.c"
static const char *step_vert = fp32_step_vert;
#include "fp32_step.geom.c"
static const char *step_geom = fp32_step_geom;
#include "fp32_draw.vert.c"
static const char *draw_vert = fp32_draw_vert;
#include "fp32_draw.frag.c"
static const char *draw_frag = fp32_draw_frag;
#include "fp32_post.vert.c"
static const char *post_vert = fp32_post_vert;
#include "fp32_post.frag.c"
static const char *post_frag = fp32_post_frag;

static const int major = 3;
static const int minor = 3;
typedef glm::vec4 vec4;
typedef glm::mat4 mat4;
typedef float flt;
#define glVertexAttribPointer_(a,b,c,d,e) glVertexAttribPointer(a,b,GL_FLOAT,c,d,e)
#define glUniformMatrix4fv_ glUniformMatrix4fv
#define glUniform1f_ glUniform1f
#define glUniform2f_ glUniform2f
#define glVertex2f_ glVertex2f

#else
#if (FP == 64)

#include "fp64_init0.vert.c"
static const char *init0_vert = fp64_init0_vert;
#include "fp64_init0.frag.c"
static const char *init0_frag = fp64_init0_frag;
#include "fp64_init1.vert.c"
static const char *init1_vert = fp64_init1_vert;
#include "fp64_init1.geom.c"
static const char *init1_geom = fp64_init1_geom;
#include "fp64_step.vert.c"
static const char *step_vert = fp64_step_vert;
#include "fp64_step.geom.c"
static const char *step_geom = fp64_step_geom;
#include "fp64_draw.vert.c"
static const char *draw_vert = fp64_draw_vert;
#include "fp64_draw.frag.c"
static const char *draw_frag = fp64_draw_frag;
#include "fp64_post.vert.c"
static const char *post_vert = fp64_post_vert;
#include "fp64_post.frag.c"
static const char *post_frag = fp64_post_frag;

static const int major = 4;
static const int minor = 0;
typedef glm::dvec4 vec4;
typedef glm::dmat4 mat4;
typedef double flt;
#define glVertexAttribPointer_(a,b,c,d,e) glVertexAttribLPointer(a,b,GL_DOUBLE,d,e)
#define glUniformMatrix4fv_ glUniformMatrix4dv
#define glUniform1f_ glUniform1d
#define glUniform2f_ glUniform2d
#define glVertex2f_ glVertex2d

#else
#error unknown precision (expected 32 or 64)

#endif
#endif

#define pi 3.141592653589793
#define BUFFER_OFFSET(i) (((unsigned char *)0)+i)
#define POSITION 0
#define COLOR 3
struct point { vec4 pos, col; };

#define D do{ int e = glGetError(); if (e != 0) { fprintf(stderr, "OpenGL error %d in %s() (line %d)\n", e, __FUNCTION__, __LINE__); } }while(0)

static const int tex_width = 512;
static const int tex_height = 512;

static int win_width = 512;
static int win_height = 512;

static GLsizei const VertexCount(1 << 24);

static flt cx = 0;
static flt cy = 0;
static flt r  = 1;
static mat4 MVP;

static int which = 0;
static GLuint PingPongArrayBufferName[2];
static GLuint PingPongVertexArrayName[2];
static GLuint InitVertexArrayName(0);

static GLuint InitVBO(0);
static GLuint InitVAO(0);

static GLuint PostVBO(0);
static GLuint PostVAO(0);

static GLuint Init0ProgramName(0);
static GLint  Init0UniformMVP(0);

static GLuint Init1ProgramName(0);
static GLint  Init1UniformCenter(0);
static GLint  Init1UniformRadius(0);

static GLuint StepProgramName(0);
static GLint  StepUniformPlusMinus(0);
static GLint  StepUniformHueShift(0);
static GLint  StepUniformPixelSpacing(0);

static GLuint DrawProgramName(0);
static GLint  DrawUniformMVP(0);

static GLuint PostProgramName(0);
static GLint  PostUniformMVP(0);
static GLint  PostUniformTex(0);

static GLuint Texture2DName(0);
static GLuint FramebufferName(0);

static GLuint Query(0);

static GLuint createShader(GLenum Type, const GLchar *Source) {
  GLuint Name = glCreateShader(Type);
  glShaderSource(Name, 1, &Source, NULL);
  glCompileShader(Name);
  return Name;
}

static bool checkProgram(GLuint ProgramName, const char *header) {
  if (!ProgramName) {
    return false;
  }
  GLint Result = GL_FALSE;
  glGetProgramiv(ProgramName, GL_LINK_STATUS, &Result);
  if (Result != GL_TRUE) {
    fprintf(stderr, "%s: link failed\n", header);
  }
  int InfoLogLength;
  glGetProgramiv(ProgramName, GL_INFO_LOG_LENGTH, &InfoLogLength);
  char *Buffer = (char *) malloc(InfoLogLength + 1); {
    glGetProgramInfoLog(ProgramName, InfoLogLength, NULL, &Buffer[0]);
    Buffer[InfoLogLength] = 0;
    if (Buffer[0]) {
      fprintf(stderr, "%s:\n%s\n", header, &Buffer[0]);
    }
  } free(Buffer);
  return Result == GL_TRUE;
}

static bool initProgram() {
  bool Validated = true;
  // Create program 'init0'
  {
    GLuint VertexShaderName = createShader(GL_VERTEX_SHADER, init0_vert);D;
    GLuint FragmentShaderName = createShader(GL_FRAGMENT_SHADER, init0_frag);D;
    Init0ProgramName = glCreateProgram();D;
    glAttachShader(Init0ProgramName, VertexShaderName);D;
    glAttachShader(Init0ProgramName, FragmentShaderName);D;
    glDeleteShader(VertexShaderName);D;
    glDeleteShader(FragmentShaderName);D;
    glLinkProgram(Init0ProgramName);D;
    Validated = Validated && checkProgram(Init0ProgramName, "init0");
    Init0UniformMVP = glGetUniformLocation(Init0ProgramName, "MVP");D;
    Validated = Validated && Init0UniformMVP != -1;
  }
  // Create program 'init1'
  {
    GLuint VertexShaderName = createShader(GL_VERTEX_SHADER, init1_vert);D;
    GLuint GeometryShaderName = createShader(GL_GEOMETRY_SHADER, init1_geom);D;
    Init1ProgramName = glCreateProgram();D;
    glAttachShader(Init1ProgramName, VertexShaderName);D;
    glAttachShader(Init1ProgramName, GeometryShaderName);D;
    glDeleteShader(VertexShaderName);D;
    glDeleteShader(GeometryShaderName);D;
    GLchar const * Strings[] = {"Position", "Color"};
    glTransformFeedbackVaryings(Init1ProgramName, 2, Strings, GL_INTERLEAVED_ATTRIBS);D;
    glLinkProgram(Init1ProgramName);D;
    Validated = Validated && checkProgram(Init1ProgramName, "init1");
    Init1UniformCenter = glGetUniformLocation(Init1ProgramName, "center");D;
    Init1UniformRadius = glGetUniformLocation(Init1ProgramName, "radius");D;
    Validated = Validated && Init1UniformCenter != -1 && Init1UniformRadius != -1;
  }
  // Create program 'step'
  {
    GLuint VertexShaderName = createShader(GL_VERTEX_SHADER, step_vert);D;
    GLuint GeometryShaderName = createShader(GL_GEOMETRY_SHADER, step_geom);D;
    StepProgramName = glCreateProgram();D;
    glAttachShader(StepProgramName, VertexShaderName);D;
    glAttachShader(StepProgramName, GeometryShaderName);D;
    glDeleteShader(VertexShaderName);D;
    glDeleteShader(GeometryShaderName);D;
    GLchar const * Strings[] = {"Position", "Color"};
    glTransformFeedbackVaryings(StepProgramName, 2, Strings, GL_INTERLEAVED_ATTRIBS);D;
    glLinkProgram(StepProgramName);D;
    Validated = Validated && checkProgram(StepProgramName, "step");
    StepUniformPlusMinus = glGetUniformLocation(StepProgramName, "plusMinus");D;
    StepUniformHueShift = glGetUniformLocation(StepProgramName, "hueShift");D;
    StepUniformPixelSpacing = glGetUniformLocation(StepProgramName, "pixelSpacing");D;
    Validated = Validated && StepUniformPlusMinus != -1 && StepUniformHueShift != -1 && StepUniformPixelSpacing != -1;
  }
  // Create program 'draw'
  {
    GLuint VertexShaderName = createShader(GL_VERTEX_SHADER, draw_vert);D;
    GLuint FragmentShaderName = createShader(GL_FRAGMENT_SHADER, draw_frag);D;
    DrawProgramName = glCreateProgram();D;
    glAttachShader(DrawProgramName, VertexShaderName);D;
    glAttachShader(DrawProgramName, FragmentShaderName);D;
    glDeleteShader(VertexShaderName);D;
    glDeleteShader(FragmentShaderName);D;
    glLinkProgram(DrawProgramName);D;
    Validated = Validated && checkProgram(DrawProgramName, "draw");
    DrawUniformMVP = glGetUniformLocation(DrawProgramName, "MVP");D;
    Validated = Validated && DrawUniformMVP != -1;
  }
  // Create program 'post'
  {
    GLuint VertexShaderName = createShader(GL_VERTEX_SHADER, post_vert);D;
    GLuint FragmentShaderName = createShader(GL_FRAGMENT_SHADER, post_frag);D;
    PostProgramName = glCreateProgram();D;
    glAttachShader(PostProgramName, VertexShaderName);D;
    glAttachShader(PostProgramName, FragmentShaderName);D;
    glDeleteShader(VertexShaderName);D;
    glDeleteShader(FragmentShaderName);D;
    glLinkProgram(PostProgramName);D;
    Validated = Validated && checkProgram(PostProgramName, "post");
    PostUniformMVP = glGetUniformLocation(PostProgramName, "MVP");D;
    PostUniformTex = glGetUniformLocation(PostProgramName, "tex");D;
    Validated = Validated && PostUniformMVP != -1 && PostUniformTex != -1;
  }
  return Validated;
}

static void initVertexArray1(GLuint *name, GLuint arr) {
  glGenVertexArrays(1, name);D;
  glBindVertexArray(*name);D; {
    glBindBuffer(GL_ARRAY_BUFFER, arr);D; {
      glVertexAttribPointer_(POSITION, 4, GL_FALSE, sizeof(point), 0);D;
    } glBindBuffer(GL_ARRAY_BUFFER, 0);D;
    glBindBuffer(GL_ARRAY_BUFFER, arr);D; {
      glVertexAttribPointer_(COLOR, 4, GL_FALSE, sizeof(point), BUFFER_OFFSET(sizeof(vec4)));D;
    } glBindBuffer(GL_ARRAY_BUFFER, 0);D;
    glEnableVertexAttribArray(POSITION);D;
    glEnableVertexAttribArray(COLOR);D;
  } glBindVertexArray(0);D;
}

static void initVertexArray() {
  initVertexArray1(&PingPongVertexArrayName[0], PingPongArrayBufferName[0]);D;
  initVertexArray1(&PingPongVertexArrayName[1], PingPongArrayBufferName[1]);D;
  glGenVertexArrays(1, &InitVertexArrayName);D;
  glBindVertexArray(InitVertexArrayName);D; {
    glBindBuffer(GL_ARRAY_BUFFER, PingPongArrayBufferName[1]);D; {
      glVertexAttribPointer(POSITION, 4, GL_FLOAT, GL_FALSE, sizeof(glm::vec4), 0);D;
    } glBindBuffer(GL_ARRAY_BUFFER, 0);D;
    glEnableVertexAttribArray(POSITION);D;
  } glBindVertexArray(0);D;
}

static void initArrayBuffer1(GLuint *name) {
  glGenBuffers(1, name);D;
  glBindBuffer(GL_ARRAY_BUFFER, *name);D; {
    glBufferData(GL_ARRAY_BUFFER, sizeof(point) * VertexCount, NULL, GL_DYNAMIC_COPY);D;
  } glBindBuffer(GL_ARRAY_BUFFER, 0);D;
}  

static GLuint resetView() {
  which = 0;
  fprintf(stderr, "%+.16g + %+.16g i @ %g (%d)\t", cx, cy, r, int(-log2(r)));
  GLuint active2;
  GLuint active = win_width * win_height;
  if (active > 0) {
    // Set the display viewport
    flt a = flt(win_width) / flt(win_height);
    MVP = glm::ortho(-a, a, flt(-1), flt(1));
    glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);D; {
      glViewport(0, 0, win_width, win_height);D;
      glUseProgram(Init0ProgramName);D; {
        glUniformMatrix4fv_(Init0UniformMVP, 1, GL_FALSE, &MVP[0][0]);D;
        glBindVertexArray(InitVAO);D; {
          glBindBuffer(GL_ARRAY_BUFFER, InitVBO);D; {
            GLfloat v[8] = { GLfloat(-a), -1, GLfloat(a), -1, GLfloat(-a), 1, GLfloat(a), 1 };
            glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(v), v);D;
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);D;
          } glBindBuffer(GL_ARRAY_BUFFER, 0);D;
        } glBindVertexArray(0);D;
      } glUseProgram(0);D;
      // copy to Pong
      glBindBuffer(GL_PIXEL_PACK_BUFFER, PingPongArrayBufferName[1]);D; {
        glReadPixels(0, 0, win_width, win_height, GL_RGBA, GL_FLOAT, 0);D;
      } glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);D;
    } glBindFramebuffer(GL_FRAMEBUFFER, 0);D;
    // transform to Ping
    glEnable(GL_RASTERIZER_DISCARD);D; {
      glUseProgram(Init1ProgramName);D; {
        glUniform1f_(Init1UniformRadius, r);D;
        glUniform2f_(Init1UniformCenter, cx, cy);D;
        glBindVertexArray(InitVertexArrayName);D; {
          glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, PingPongArrayBufferName[0]);D; {
            glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, Query);D; {
              glBeginTransformFeedback(GL_POINTS);D; {
                glDrawArrays(GL_POINTS, 0, active);D;
              } glEndTransformFeedback();D;
            } glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);D;
            glGetQueryObjectuiv(Query, GL_QUERY_RESULT, &active2);D;
          } glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);D;
        } glBindVertexArray(0);D;
      } glUseProgram(0);D;
    } glDisable(GL_RASTERIZER_DISCARD);D;
  }
  return active;
}

static void initArrayBuffer() {
  initArrayBuffer1(&PingPongArrayBufferName[0]);D;
  initArrayBuffer1(&PingPongArrayBufferName[1]);D;
}

static int calculate1(GLuint active, int pass) {
  if (active <= 0) return 0;
  GLuint kept1 = 0;
  GLuint kept2 = 0;
  glEnable(GL_RASTERIZER_DISCARD);D; {
    glUseProgram(StepProgramName);D; {
      glUniform1f_(StepUniformPixelSpacing, r * sqrt(2.0) / win_height);D;
      glBindVertexArray(PingPongVertexArrayName[which]);D; {

        glUniform1f_(StepUniformPlusMinus,  1);D;
        glUniform1f_(StepUniformHueShift,   pi * pow(0.5, pass + 1));D;
        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, PingPongArrayBufferName[1 - which]);D; {
          glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, Query);D; {
            glBeginTransformFeedback(GL_POINTS);D; {
              glDrawArrays(GL_POINTS, 0, active);D;
            } glEndTransformFeedback();D;
                } glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);D;
                glGetQueryObjectuiv(Query, GL_QUERY_RESULT, &kept1);D;
        } glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);D;

        glUniform1f_(StepUniformPlusMinus, -1);D;
        glUniform1f_(StepUniformHueShift,  -pi * pow(0.5, pass + 1));D;
        glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 0, PingPongArrayBufferName[1 - which], kept1 * sizeof(point), (VertexCount - kept1) * sizeof(point));D; {
          glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, Query);D; {
            glBeginTransformFeedback(GL_POINTS);D; {
              glDrawArrays(GL_POINTS, 0, active);D;
            } glEndTransformFeedback();D;
          } glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);D;
          glGetQueryObjectuiv(Query, GL_QUERY_RESULT, &kept2);D;
        } glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);D;

      } glBindVertexArray(0);D;
    } glUseProgram(0);
    which = 1 - which;
  } glDisable(GL_RASTERIZER_DISCARD);D;
  return kept1 + kept2;
}

static void draw(GLuint active) {
  flt a = flt(win_width) / flt(win_height);
  MVP = glm::ortho(cx - a * r, cx + a * r, cy - r, cy + r);
  glViewport(0, 0, win_width, win_height);D;
  glEnable(GL_BLEND);D; {
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);D;
    glUseProgram(DrawProgramName);D; {
      glUniformMatrix4fv_(DrawUniformMVP, 1, GL_FALSE, &MVP[0][0]);D;
      glBindVertexArray(PingPongVertexArrayName[which]);D; {
        glDrawArrays(GL_POINTS, 0, active);D;
      } glBindVertexArray(0);D;
    } glUseProgram(0);D;
  } glDisable(GL_BLEND);D;
}

static GLuint calculate(GLuint active) {
  int pass = 0;
  while (2 * active <= GLuint(VertexCount) && pass < 256) {
    active = calculate1(active, pass);
    pass += 1;
  }
  fprintf(stderr, "%9d\t%d\t", active, pass);
  return active;
}

static void render() {
  GLuint active = 0;
  active = resetView();
  active = calculate(active);
  glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);D; {
    glClear(GL_COLOR_BUFFER_BIT);
    draw(active);
  } glBindFramebuffer(GL_FRAMEBUFFER, 0);D;
  flt x = flt(win_width  - 1) / flt(tex_width);
  flt y = flt(win_height - 1) / flt(tex_height);
  MVP = glm::ortho(flt(0), x, flt(0), y);
  glUseProgram(PostProgramName);D; {
    glBindTexture(GL_TEXTURE_2D, Texture2DName);D; {
      glUniformMatrix4fv_(PostUniformMVP, 1, GL_FALSE, &MVP[0][0]);D;
      glUniform1i(PostUniformTex, 0);D;
      glBindVertexArray(PostVAO);D; {
        glBindBuffer(GL_ARRAY_BUFFER, PostVBO);D; {
          GLfloat v[8] = { 0, 0, GLfloat(x), 0, 0, GLfloat(y), GLfloat(x), GLfloat(y) };
          glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(v), v);D;
          glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);D;
        } glBindBuffer(GL_ARRAY_BUFFER, 0);D;
      } glBindVertexArray(0);D;
    } glBindTexture(GL_TEXTURE_2D, 0);D;
  } glUseProgram(0);D;
  glutSwapBuffers();
}

static bool begincb() {
  bool Validated = true;
  // glClampColor(GL_CLAMP_VERTEX_COLOR, GL_FALSE);D;
  glClampColor(GL_CLAMP_READ_COLOR, GL_FALSE);D;
  // glClampColor(GL_CLAMP_FRAGMENT_COLOR, GL_FALSE);D;

  glGenQueries(1, &Query);D;
  Validated = Validated && initProgram();
  initArrayBuffer();
  initVertexArray();

  glGenBuffers(1, &PostVBO);D;
  glGenVertexArrays(1, &PostVAO);D;
  glBindVertexArray(PostVAO);D; {
    glBindBuffer(GL_ARRAY_BUFFER, PostVBO);D; {
      glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);D;
      glVertexAttribPointer(POSITION, 2, GL_FLOAT, GL_FALSE, 0, 0);D;
      glEnableVertexAttribArray(POSITION);D;
    } glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  } glBindVertexArray(0);D;
  glGenBuffers(1, &InitVBO);D;
  glGenVertexArrays(1, &InitVAO);D;
  glBindVertexArray(InitVAO);D; {
    glBindBuffer(GL_ARRAY_BUFFER, InitVBO);D; {
      glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(GLfloat), 0, GL_DYNAMIC_DRAW);D;
      glVertexAttribPointer(POSITION, 2, GL_FLOAT, GL_FALSE, 0, 0);D;
      glEnableVertexAttribArray(POSITION);D;
    } glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  } glBindVertexArray(0);D;

  glGenTextures(1, &Texture2DName);D;
  glBindTexture(GL_TEXTURE_2D, Texture2DName);D;
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, tex_width, tex_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);D;
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glBindTexture(GL_TEXTURE_2D, 0);D;
  glGenFramebuffers(1, &FramebufferName);D;
  glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);D;
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, Texture2DName, 0);D;
  GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(1, DrawBuffers);D;
  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    fprintf(stderr, "frame buffer %d\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
  }
  glBindFramebuffer(GL_FRAMEBUFFER, 0);D;
  return Validated;
}

static void endcb() {
  // FIXME clean up gracefully!
  // glDeleteVertexArrays(1, &va);
  // glDeleteBuffers(1, &ab);
  // glDeleteProgram(p);
  // glDeleteQueries(1, &q);
}

static void displaycb(void) {
  clock_t start = clock();
  glClear(GL_COLOR_BUFFER_BIT);
  render();
  clock_t end = clock();
  double elapsed = 1000.0 * ((double) (end - start)) / CLOCKS_PER_SEC;
  fprintf(stderr, "%dms\n", int(elapsed));
  glutSwapBuffers();
}

static void reshapecb(int w, int h) {
  if (w > tex_width || h > tex_height) {
    fprintf(stderr, "warning: window size too big\n");
  }
  win_width = w;
  win_height = h;
  glutPostRedisplay();
}

static void mousecb(int button, int state, int x, int y) {
  if (state == GLUT_DOWN) {
    if (button == GLUT_LEFT_BUTTON) {
      cx += 2 * r * (x -  win_width / 2.0) / win_height;
      cy += 2 * r * (win_height / 2.0 - y) / win_height;
      r  *= 0.5;
      glutPostRedisplay();
    } else if (button == GLUT_MIDDLE_BUTTON) {
      cx += 2 * r * (x -  win_width / 2.0) / win_height;
      cy += 2 * r * (win_height / 2.0 - y) / win_height;
      r  *= 1.0;
      glutPostRedisplay();
    } else if (button == GLUT_RIGHT_BUTTON) {
      cx += 2 * r * (x -  win_width / 2.0) / win_height;
      cy += 2 * r * (win_height / 2.0 - y) / win_height;
      r  *= 2.0;
      glutPostRedisplay();
    }
  }
}

static void keyboardcb(unsigned char key, int x, int y) {
  (void) x;
  (void) y;
  if (key == 27) {
    exit(0);
  }
}

static void closecb() {
  exit(0);
}

int main(int argc, char* argv[]) {
  glutInitWindowSize(win_width, win_height);
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);

  int WindowHandle = glutCreateWindow(argv[0]);
  glewInit();
  glutDestroyWindow(WindowHandle);

  glutInitContextVersion(major, minor);
  if (100 * major + 10 * minor >= 320) {
//    glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE | GLUT_DEBUG);
  }
  glutCreateWindow(argv[0]);

  if (! begincb()) { return 1; }

  glutDisplayFunc(displaycb);
  glutReshapeFunc(reshapecb);
  glutMouseFunc(mousecb);
  glutKeyboardFunc(keyboardcb);
  glutCloseFunc(closecb);
  atexit(endcb);
  glutMainLoop();
  return 0;
}
